Rails.application.routes.draw do

  get 'shifts/create'

  get 'shifts/destroy'

  get 'sessions/new'



resources :users
resources :events
resources :shifts do
	member do
		post :reject
		post :confirm
		post :accepted
	end
end

root 'static_pages#home'

get 'help' => 'static_pages#help'

get 'about' => 'static_pages#about'

get 'login' => 'sessions#new'

post 'login' => 'sessions#create'

delete 'logout' => 'sessions#destroy'

end
