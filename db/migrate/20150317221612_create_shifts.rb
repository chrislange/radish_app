class CreateShifts < ActiveRecord::Migration
  def change
    create_table :shifts do |t|
      t.string :name
      t.string :location
      t.date :date
      t.time :time
      t.references :event, index: true
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :shifts, :events
    add_foreign_key :shifts, :users
  end
end
