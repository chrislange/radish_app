class ShiftsController < ApplicationController

  def new
  end

  def create
  		@shift = Shift.new(shift_params)
  		@event = Event.find(params[:shift][:event_id])
  		@user = User.find(params[:shift][:user_id])
  		@shift.event_id = @event.id
  		@shift.user_id = @user.id
  		@shift.location = @event.location
  		@shift.name = @event.name
      @shift.status = :pending

  		if @shift.save 
			 redirect_to @event
       else
          flash.now[:danger] = 'Invalid Shift'
          render 'new'
		end

  end

 
  def update
    @shift = Shift.find(params[:id])
    @event = @shift.event
    @shift.update_attributes(shift_params)
      redirect_to @event
  end

  def destroy
  	@shift = Shift.find(params[:id])
  	@event = @shift.event
  	@shift.destroy

  	redirect_to @event
  end

def accepted
     @shift = Shift.find(params[:id])
      @event = @shift.event
      @user = User.find_by(id: @shift.user_id)
      @shift.status = :accepted
      if @shift.save 
        redirect_to root_path
      else 
        
      end
  end

  def confirm
     @shift = Shift.find(params[:id])
      @event = @shift.event
      @user = User.find_by(id: @shift.user_id)
      @shift.status = :confirmed
      if @shift.save 
        redirect_to root_path
      else 
        
      end
  end

  def reject
      @shift = Shift.find(params[:id])
      @event = @shift.event
      @user = User.find_by(id: @shift.user_id)
      @shift.status = :rejected
      
      if @shift.save 
        redirect_to root_path
      else 
        
      end
  end

  private 

  def shift_params 

  	params.require(:shift).permit(:time, :location, :date, :user_id, :event_id, :status)

  end
end
