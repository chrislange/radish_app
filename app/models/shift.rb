class Shift < ActiveRecord::Base
	
  belongs_to :event
  belongs_to :user

  scope :accepted, -> { where(status: 'accepted') }
  scope :confirmed, -> { where(status: 'confirmed') }
  scope :needsconfirmation, -> { where(status: 'needsconfirmation') }
  scope :rejected, -> { where(status: 'rejected') }
  scope :pending, -> { where(status: 'pending') }
end
