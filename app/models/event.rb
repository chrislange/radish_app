class Event < ActiveRecord::Base
	has_many :shifts, dependent: :destroy
	has_many :users, through: :shifts

	validates :name, presence: true
	validates :date, presence: true
end
